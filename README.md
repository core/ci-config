# GitLab CI Configuration for standard DUNE modules

This repository contains common CI configuration files for standard DUNE modules. For now, this
mostly means job definitions. These files are included by the `.gitlab-ci.yml` files of the
individual modules, which makes it a lot easier to adjust the CI setup by centralizing the
configuration.
